# Fermi/GBM
Fermi_GBM_Erange=[0.01, 1] # Energy range in MeV
Fermi_GBM_sens=2.28#trigger threshold in ph/cm2/s
Fermi_GBM_flux_unit='ph'
Fermi_GBM_window=64e-3 #width of photon/energy accumulation window, in s
Fermi_GBM_bkg=.456 #ph/cm2/s
Fermi_GBM_FoV=0.6 # Field of View
Fermi_GBM_corr=2
# Swift/BAT
Swift_BAT_Erange=[0.015, 0.15] # Energy range
Swift_BAT_sens=1.5#0.9
Swift_BAT_flux_unit='ph'
Swift_BAT_window=20*1e-3 #20 ms
Swift_BAT_bkg=.3 #ph/s/cm2
Swift_BAT_FoV=0.1
Swift_BAT_corr=3
# Konus
Konus_Erange=[0.02, 15] 
Konus_sens=1e-7
Konus_FoV=0.8
Konus_corr=1
# insight/HE
insight_HE_Erange=[0.03, 0.1] 
insight_HE_sens=2e-8
insight_HE_FoV=0.6
insight_HE_corr=1
# GECAM
GECAM_Erange=[0.05, .4]#[0.006, 5]
GECAM_sens=2.0e-8
GECAM_flux_unit='ergs'
GECAM_window=50e-3
GECAM_bkg=0.4e-8
GECAM_FoV=1.
GECAM_corr=3.5
# SVOM/GRM
SVOM_GRM_Erange=[0.03, 5]
SVOM_GRM_sens=2e-8
SVOM_GRM_FoV=0.2
SVOM_corr=1
# SVOM/ECLAIRs
SVOM_ECLAIRs_Erange=[0.004, 0.25]
SVOM_ECLAIRs_sens=2e-8
SVOM_ECLAIRs_FoV=0.2
SVOM_corr=1
# BATSE
BATSE_Erange=[0.05, .3]
BATSE_sens=.3#photons/cm2/s
BATSE_flux_unit='ph'
BATSE_window=1
BATSE_bkg=.06 #ph/s/cm2
BATSE_FoV=.25
BATSE_corr=1


