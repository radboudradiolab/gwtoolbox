# model parameters of sGRBs
pi=3.1415926
half_open_mean_=15.*pi/180.
half_open_std_=4.*pi/180.
lg_r_grb_mean_=13
lg_r_grb_std_=.1
lgEgrb_mean_=49.3
lgEgrb_std_=.5
nup0_mean_=0.001
nup0_std_=0.00075
jet_Gamma_mean_=250
jet_Gamma_std_=75
Band_alpha=-1.
Band_beta=-2.2
Band_s=1
t_d_sigma=0.4
