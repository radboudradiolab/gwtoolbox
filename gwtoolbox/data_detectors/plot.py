import numpy as np
import matplotlib.pyplot as plt

# 从文本文件中读取数据
data1 = np.loadtxt('aVirgo.txt')  # 请将'your_file.txt'替换为你的文件路径
data2 = np.loadtxt('aLIGO.txt')
data3 = np.loadtxt('aLIGO_o4.txt')
# 提取两列数据
x1 = data1[:, 0]  # 假设第一列是 x 数据
y1 = data1[:, 1]  # 假设第二列是 y 数据
x2 = data2[:, 0]  # 假设第一列是 x 数据
y2 = data2[:, 1]  # 假设第二列是 y 数据
x3 = data3[:, 0]  # 假设第一列是 x 数据
y3 = data3[:, 1]  # 假设第二列是 y 数据

# 绘制图表
plt.plot(x1, y1, label='Virgo_o3b')
plt.plot(x2, y2, label='LIGO_o3')
plt.plot(x3, y3, label='LIGO_o4')
plt.legend()
plt.xlabel('X轴标签')
plt.ylabel('Y轴标签')
plt.title('数据图表')
plt.xscale('log')
plt.yscale('log')
plt.show()
plt.savefig('sensitivity.png')

