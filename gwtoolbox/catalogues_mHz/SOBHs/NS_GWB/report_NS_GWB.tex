\documentclass[twoside, a4paper, 12pt]{report}
\usepackage{graphicx,epsfig,color} 
\usepackage{amssymb,amsmath} 
\usepackage{pstricks} 
%\usepackage[dark,timestamp]{draftcopy}

% definitions 
\def\r{\textcolor{red}} 
\def\b{\textcolor{blue}}
\newcommand{\msun}{M_{\odot}}
\begin{document}

\section*{Extragalactic foregrounds in the LISA band}

\centerline{A. Sesana}

\centerline{(26 October 2017)}

\vspace{0.5cm}

{\it The following note is just a re-invention of the wheel, where 'the wheel' can be found in Farmer \& Phinney (2003, MNRAS 346 1197). However, following the last aLIGO-Virgo detections, we can use the inferred rates to estimate the expected binary neutron star (BNS) stochastic gravitational wave background (GWB). Since, to the best of my knowledge, this has not been mentioned within the LISA WP discussion (and it does not appear in the WP document yet), this is a reminder that we should not forget about it.}

\vspace{0.3cm}

The recent detection of GW170817 allowed an estimate of the local merger rate of BNSs. The inferred number is $1540^{+3200}_{-1220}$Gpc$^{-3}$yr$^{-1}$ (Abbott et al. 2017, PRL 119 10.1103). This is $\approx 15$ higher than the inferred local merger rate of binary black holes (BBHs), set to be approximately $103^{+110}_{-63}$Gpc$^{-3}$yr$^{-1}$, assuming a Salpeter mass function for the primary hole in the range $5-95\msun$ and a flat distribution for the mass of the secondary hole. For such distribution, the average binary chirp mass is ${\cal M}\approx 6\msun$, which is just a factor of $\sim 5$ larger than the typical BNS chirp mass ${\cal M}\approx 1.2\msun$. Since the scaling of the stochastic GWB is\begin{equation}
  h_{c,\rm bkg}\propto{\cal M}^{5/6}\sqrt{\dot{N}},
\end{equation}
where ${\cal M}$ is the typical chirp mass and $\dot{N}$ is the merger rate, it follows that the BNS GWB must be comparable to (actually slightly smaller than) the BBH GWB.

The signal-to-noise ratio (S/N) of a stochastic GWB is estimated following Thrane \& Romano (2013, PRD 88 4032) as
\begin{equation}
  ({\rm S/N})^2_{\rm bkg} = T\int\gamma(f)\frac{h_{c,\rm bkg}^4(f)}{4f^2\langle S(f)\rangle^2}df,
  \label{snrbkg}
\end{equation}
where $T=4\,$yr is the mission lifetime and $h_{c,\rm bkg}^2(f)=fS_h(f)$, being $S_h(f)$ the power spectral density of the signal. Note that the response function $\gamma(f)\approx 1$ in the relevant frequency range (see figure 4 in Thrane \& Romano 2013). $h_{c,\rm bkg}$ is related to the GW energy density via $h_{c,\rm bkg}^2=3H_0^2\Omega_{\rm GW}^2/(2\pi^2f^2)$.

In the practical calculation, the LISA $S(f)$ is taken from the accepted Consortium proposal as in Amaro-Seoane et al. (2017, arXiv:1702.00786). The signal is obtained by summing in quadrature the characteristic strains of all sources up to $z=5$, assuming a constant merger rate equal to the local estimated one. Note that this is a {\it conservative} assumption, since the compact object (both BNS and BBH) merger rate has been estimated to increase with redshift at least up to $z\approx 2$, tracing the temporal evolution of the cosmic star formation (e.g. Vangioni et al. 2015, MNRAS 447 2575). In fact, when extrapolated to the aLIGO frequencies, the median BBH GWB estimated here is a factor $1.5-2$ (in $\Omega_{\rm GW}$) smaller than what computed by the LIGO-Virgo collaboration. 

Results are shown in figure \ref{example}. Note that I did not calculate the $\Omega_{\rm GW}$ sensitivity curve 'a la Thrane \& Romano' for the new LISA design, so old curves computed by A. Petiteau for the N2A1 and N2A5 GOAT study baselines are shown for comparison. The median estimate of $\Omega_{\rm BNS}$ is only a factor of $\approx 2$ smaller than $\Omega_{\rm BBH}$, indicating that this will also be an important foreground for LISA. The two foregrounds will likely not be separable, so the sum of the two should be considered. The 90\% region for the BNS GWB is a factor of two larger, because of the larger uncertainties associated with a single BNS detection (whereas we have a good 4.8 BBH!!!). Table \ref{tab1} shows the associated S/N computed via equation (\ref{snrbkg}). Even if the BNS GWB turns out to be at the lower end of the 90\% credible interval estimated here, it should be marginally detectable by LISA (and keep in mind this estimate is likely a lower limit, because of the assumption of non-evolving BNS merger rate).

%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\begin{center}
\begin{tabular}{c|ccc}
\hline 
Source &  lower$90\%$ & median & upper$90\%$\\
\hline
BBH      &  13.8  &  34.5  & 72.5 \\
BNS      &   3.8  &  19.0  & 63.4 \\
\hline
\end{tabular}
\end{center}
\caption{S/N of the BBH and BNS stochastic GWB for the current LISA baseline design.}
\label{tab1}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=13.0cm,clip=true,angle=0]{fig_NS_BH_GWB.pdf}
\caption{$\Omega_{\rm GW}$ as a function of frequency. The blue and red curves represent the expected signal from BBHs and BNSs respectively. Solid lines are the medians and dashed lines the 90\% credible intervals calculated from the current rates inferred by aLIGO-Virgo observations. The black lines represent GWB sensitivity curves for 5yr of integration for two old LISA design proposals (shown for comparison).}
\label{example}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
