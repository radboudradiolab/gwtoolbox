Detectors
=========

GW detectors description

Earth detectors
---------------

.. automodule:: gwtoolbox.detectors_earth

  .. autoclass:: gwtoolbox.detectors_earth.LigoLike
     :members:

  .. autoclass:: gwtoolbox.detectors_earth.ETLike
     :members:


Simulate LIGO-like detector
***************************

.. automodule:: gwtoolbox.simulate_ligo

  .. autoclass:: gwtoolbox.simulate_ligo.Session
     :members:


Space detectors
---------------

.. automodule:: gwtoolbox.detectors_space

  .. autoclass:: gwtoolbox.detectors_space.LisaLike
     :members:
