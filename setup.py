from setuptools import setup
#dependencies = ['numpy','scipy','astropy','matplotlib','pykat','common','pyIMRPhenomD','FastBinary','AAKwrapper']
dependencies = ['numpy','scipy','astropy','matplotlib','pykat']
setup(name='gwtoolbox',
      version='0.1',
      description='Gravitational Wave Tools',
      url='https://gw-universe.org/',
      packages=['gwtoolbox'],
      package_data={'gwtoolbox': ['data_detectors/*.txt','*.rst','*.dat','*.txt','accessory/*.txt','accessory/*.dat','catalogues_mHz/*']},
      scripts=[],
      install_requires=dependencies)
